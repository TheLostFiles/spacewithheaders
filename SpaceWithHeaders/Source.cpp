#include <iostream>
#include "Story.h"


using namespace std;

void main()
{
	Space objects_space;
	
	int userChoice;
	bool playAgain = false;
	
	do
	{
		objects_space.Intro(); // runs intro
		objects_space.Choices(); // runs choices

		cout << "Play again for more endings.\n";
		cout << "Would you like to play again?\n";
		cout << "1 - Yes\n";
		cout << "2 - No\n";

		userChoice = objects_space.GetNumbersFromUser(); // sets user choice to whatever the user inputs
		
		if (userChoice == 1) // checks the user choice
		{
			playAgain = true; // sets bool
		}
		else 
		{
			playAgain = false; // sets bool
		}
	}
	while (playAgain); // checks is bool is true
	
	system("CLS");
}
