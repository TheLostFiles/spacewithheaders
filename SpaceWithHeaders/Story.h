#pragma once
#include <string>
using namespace std;

class Space
{
	public:
	
		string GetTextFromUser(); // text function
		int GetNumbersFromUser(); // numbers functions
	
		void Intro(); // prototypes function
		void Choices();// prototypes function
		void Panels();// prototypes function
		void Suit();// prototypes function
		void Port();// prototypes function
		void Door();// prototypes function
		
	private:
	
		string text; // string for my text
		int choicenum; // int for my choices
		int &Ending = End; // sets the number of endings
		int End = 0;

		bool suit = false; // sets a bool for if you've been over to the suit
		bool suitOn = false; // sets a bool if you put on the suit
		bool port = false; // sets a bool for if you've been to the port
};

class babySpace : Space
{
public:
	void End(int Ending);// prototypes function
private:
};

